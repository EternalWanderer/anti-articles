---
title: "Darkweb/Darknet"
date: 2023-07-26T18:49:44+02:00
author: Marty Sluijtman
sources: [ "https://torproject.org", "https://letsdecentralize.org" ]
documentSource:
tags: [ "internet", "privacy", "security" ]
description: "Introductie tot het concept van verschillende informatie lagen op het internet"
toc: true
---

# Introductie

Waarschijnlijk heb je wel eens gehoord van het enge darknet. Soms verschijnt er
wel eens iets over in het nieuws omdat er een drugsmarkt op het internet
neergehaald is. Soms gaat het om een pedofiel die opgepakt is na jaren
opsporingswerk en samenwerking tussen verschillende landen. Het beeld wat je
hierdoor van het darknet kunt krijgen is dat het alleen maar gebruikt wordt door
pedofielen en drugdealers. Dit klopt niet en het is -- naar mijn mening --
vooral een tactiek om mensen van darknets af te houden door het te associëren
met de meest afschuwelijke uitschotten die de mensheid te bieden heeft. Darknets
zijn uiteindelijk alleen maar tools, net als het clearnet -- het gewone
internet.

# Terminologie

Maar laat ik een paar stappen terugnemen om veel rondgegooide termen uit te
leggen. In het nieuws en op het algemene internet kom je regelmatig -- al dan
niet meestal -- incorrecte toepassingen tegen van termen als darknet en deepnet;
en clearnet zul je waarschijnlijk niet eens tegenkomen onder "normale"
omstandigheden.

## Clearnet/Clearweb

Alles wat niet tot het darknet hoort.

Zowel de publiekelijk internet en deepweb categorieën horen hieronder.

## Publiek internet

Het publieke internet is alles wat er te vinden is via search engines als
Google, DuckDuckGo, Bing en Yandex. Dit zijn sites zoals YouTube, Wikipedia en
de eerder genoemde search engines zelf.

## Deepnet/Deepweb

Delen van het internet die niet publiekelijk te vinden zijn. Dit kan zijn omdat het
niet geïndexeerd is door grote search engines of omdat je een account nodig hebt
om erbij te kunnen.

Je Gmail is een voorbeeld van wat onder de deepweb categorie valt. Een ander
voorbeeld zijn je social media feeds.

Een voorbeeld van wat "deepnet" is omdat het niet geïndexeerd is zijn slecht
beveiligde file servers. Het is mogelijk om er bij te komen en alles van te
downloaden zonder een account, maar waarschijnlijk zul je ze niet tegenkomen
omdat er (vrijwel) geen publieke referenties naar zijn.

## Darknet/Darkweb

Delen van het internet die alleen te bereiken zijn met specifieke programma's.
Dit is wat er door de meeste mensen bedoelt wordt als ze het over deepnet
hebben.

Wat je ook kunt tegenkomen is dat de term darknet gebruikt wordt om de categorie
van het gebruikte protocol te beschrijven. Voorbeelden volgen hierna.

Het doel van een darknet is het toestaan van anonimiteit binnen een systeem wat
van zichzelf alles bijhoudt; op het clearnet wordt alles, maar dan ook echt
_alles_ gelogd.

Het doel van een darknet is het verbergen van het verkeer wat erop plaatsvind.
Dit gebeurt door het verkeer nooit direct van de gebruiker naar de server te en
terug laten gaan.

De bekendste modellen maken gebruik van verschillende hops of nodes -- stappen
die het verkeer maakt voordat het het einddoel bereikt.

Hierdoor is darknet verkeer vrijwel altijd een stuk trager dan clearnet verkeer.

# Gebruik

> "Waarom zou ik een/het darknet willen gebruiken?"

Er zijn heel wat redenen om een darknet te gebruiken. Als je bijvoorbeeld iets
met betrekking tot medisch informatie wilt opzoeken en dat niet wilt laten
lekken aan overheden en grote bedrijven is het gebruik van een darknet ideaal.

Een ander voorbeeld is als je als gevoelige informatie hebt over je werkplek.
Deze informatie wil je naar buiten krijgen zonder dat het direct naar jou terug
getraceerd wordt. Redelijk wat nieuwsorganisaties hebben tegenwoordig darknet
dropoff punten om dit te faciliteren.

Een andere reden kan zijn omdat het verkeer binnen het netwerk waar je op dat
moment zit gecensureerd wordt.[^1]

Op een bepaalde manier is een darknet sessie (al dan vooral [Tor](#tor)) te
vergelijken met een incognito[^2] sessie in je gewone browser. Alleen is het
_daadwerkelijk_ een incognito sessie.

Daarnaast is er ook het interessante fenomeen dat darknets -- wegens de
technologieën die erbinnen worden gebruikt -- in de zekere zin een reflectie zijn
van het internet rond 2010. Hierdoor zijn er heel wat mensen die in deze stijl
websites maken die op het clearnet lastiger te vinden zijn; ook met search
engines.

## Tor

Tor is het meest bekende/beruchte darknet. Als je het in het nieuws hoort over
darknet gaat het vrijwel altijd over Tor. Tor staat voor The Onion Router. Dit
is een knipoog naar hoe het protocol werkt.

Om uit te leggen hoe het Tor netwerk werkt, moet ik eerst heel globaal een
stukje uitleggen over hoe het gewone internet werkt.

Als je als gebruiker naar, bv. `google.com` wilt gaan, stuurt je browser een
vraag -- een request -- voor `https://google.com` naar de service die je request
doorstuurt naar de site waar je het wil hebben -- je DNS provider.  
De taak van de DNS -- Domain Name Service -- provider is om een lijst van
domeinen en bijbehorende IP adressen bij te houden.  
Je DNS provider geeft vervolgens je request door aan het IP wat bij `google.com`
hoort. De server die op dat IP adres luistert, geeft vervolgens `index.html`
terug als antwoord op je request. Er zit nog heel wat meer complexiteit aan
vast, maar hier komt het in vogelvlucht op neer.

```goat
   gebruiker     |          internet           |     einddoel    
-----------------------------------------------------------------
                 |                             |                 
  google.com     |                             |                 
                 |                             |                 
     +---+       |           +-----+           |      +---+      
     | A +------->-----------+ DNS +----------->------+ B |      
     +---+       |           +-----+           |      +---+      
                 |                             |                 
                 |                             |  142.251.36.46  
                 |                             |                 
                 |                             |                 

```

Wat Tor anders maakt is dat je verkeer eerst via drie nodes gaat voordat het een
DNS provider bereikt. Deze nodes zijn random gekozen en verspreid over de hele
wereld. Daarnaast heeft je initiële request drie lage encryptie die er één per
node af gepeld worden, waardoor er nooit één punt in de verbinding buiten je
browser om het hele pad weet. Vandaar de naam Onion Router -- uien hebben lagen.

Hier een voorbeeldje van hoe verkeer naar duckduckgo.com kan gaan (duck.com
verweist je door naar duckduckgo.com)
```goat
                                                                    'gewoon'                    
    gebruiker    |                  Tor netwerk                   | internet |    einddoel      
------------------------------------------------------------------------------------------------
                 |   +--------+     +--------+     +--------+     |          |                  
                 |   | node 1 |  +--+ node 2 +--+  | node 3 |     |          |                  
                 |   +--------+  |  +--------+  |  +--------+     |          |                  
    duck.com     |               ^              |                 |          |                  
                 |               |              |                 |          |                  
     +---+       |   +--------+  |  +--------+  |  +--------+     | +-----+  |      +---+       
     | A +------->---+ node 1 +--+  | node 2 |  v  | node 3 |  +-->-+ DNS +-->------+ B |       
     +---+       |   +--------+     +--------+  |  +--------+  |  | +-----+  |      +---+       
                 |                              |              |  |          |                  
                 |                              |              ^  |          |  52.142.124.215  
                 |   +--------+     +--------+  |  +--------+  |  |          |                  
                 |   | node 1 |     | node 2 |  +--+ node 3 +--+  |          |                  
                 |   +--------+     +--------+     +--------+     |          |                  
```

Daarnaast heeft Tor ook zogeheten hidden services -- ook wel onions genoemt.
Deze hidden services worden alleen binnen het Tor netwerk gehost. Om deze te
bereiken moet je dus per se de Tor software op je machine draaien.

Hidden services zijn te bereiken met `.onion` domeinen. De structuur van deze
domeinen is altijd 56 (bijna) random characters gevold door `.onion`.

Hier zijn een paar "onions":

- [`antizevcdqbaz4qkdp6xfhm7dvzw7llwiktknoa3u5s3zwksdmue32id.onion`](http://antizevcdqbaz4qkdp6xfhm7dvzw7llwiktknoa3u5s3zwksdmue32id.onion) -- Deze
  website
- [`bbcweb3hytmzhn5d532owbu6oqadra5z3ar726vq5kgwwn6aucdccrad.onion`](http://bbcweb3hytmzhn5d532owbu6oqadra5z3ar726vq5kgwwn6aucdccrad.onion) -- BBC
  International
- [`xanthexikes7btjqlkakrxjf546rze2n4ftnqzth6qk52jdgrf6jwpqd.onion`](http://xanthexikes7btjqlkakrxjf546rze2n4ftnqzth6qk52jdgrf6jwpqd.onion) -- LetsDecentralize

Dit is hoe je via de onion van de BBC een verbinding naar hun server zou kunnen
maken met de Tor Browser.

Omdat er aan de kant van de server dezelfde software draait als aan de kant van
je browser, zitten er drie nodes extra tussen voordat je de server bereikt.

```goat
                                                                                                                                     
    gebruiker    |                                          Tor netwerk                                          |     einddoel      
-------------------------------------------------------------------------------------------------------------------------------------
                 |     +--------+     +--------+     +--------+     +--------+     +--------+     +--------+     |                   
                 |  +--+ node 1 +-->--+ node 2 +--+  | node 3 |  +--+ node 4 +--+  | node 5 |  +--+ node 6 +--+  |                   
                 |  |  +--------+     +--------+  |  +--------+  |  +--------+  |  +--------+  |  +--------+  |  |                   
    .onion       |  |                             |              |              |              ^              |  |                   
                 |  |                             |              |              v              |              |  |                   
     +---+       |  |  +--------+     +--------+  |  +--------+  |  +--------+  |  +--------+  |  +--------+  |  |       +---+       
     | A +------->--+  | node 1 |     | node 2 |  v  | node 3 |  ^  | node 4 |  +--| node 5 +--+  | node 6 |  +-->-------+ B |       
     +---+       |     +--------+     +--------+  |  +--------+  |  +--------+     +--------+     +--------+     |       +---+       
                 |                                |              |                                               |                   
                 |                                |              |                                               |     127.0.0.1     
                 |     +--------+     +--------+  |  +--------+  |  +--------+     +--------+     +--------+     |                   
                 |     | node 1 |     | node 2 |  +--+ node 3 +--+  | node 4 |     | node 5 |     | node 6 |     |                   
                 |     +--------+     +--------+     +--------+     +--------+     +--------+     +--------+     |                   
```
**NOTE:** wegens hoe Tor op servers geconfigureerd wordt gaat alles aan de
server kant via `127.0.0.1`.

Het voordeel aan deze hidden services is dat ze inherent geen gebruik maken van
grote services als DNS providers.

> "Hoe kan ik het Tor netwerk benaderen?"

### Tor browser

De aangeraden methode is met gebruik van de Tor browser. Deze is te downloaden
via [torpoject.org](https://torproject.org). De instructies hier zijn vrij
duidelijk.

Als je Tor eenmaal opgestart hebt zul je gegroet worden met een venster wat
redelijk lijkt op dat van Firefox. Dit komt omdat de Tor browser een Firefox
fork is.

Voordat je met ook maar iets doet, raad ik aan de security modus aan te passen.
Dit doe je door rechts boven op het schildje en vervolgens "settings" te
klikken. Dit brengt je naar een menu met drie opties:

- Standard
- Safer
- Safest

Kies hiervan "Safest".

Dit zal ervoor zorgen dat de nodige websites niet fatsoenlijk kunnen laden omdat
dit onder andere JavaScript uitzet. De reden hiervoor is dat JavaScript
regelmatig misbruikt wordt om je online gedrag te traceren. Zonder JavaScript is
dit een stuk lastiger.

Verder is er nog het gebruikelijke advies van "zet het Tor browser window nooit
op fullscreen". De gedachten hierachter is dat je beeldschermresolutie te
achterhalen is zonder JavaScript en daarmee getraceerd kan worden. Hierdoor is
dit vooral relevant als je een monitor hebt met een minder vaak voorkomende
resolutie. [Statistisch
gezien](https://gs.statcounter.com/screen-resolution-stats/desktop/worldwide)
heb je een 1920x1080 monitor. Hetzelfde geldt dus ook voor Tor gebruikers. Als
je hiermee je Tor window op fullscreen zet behoor je dus aantoonbaar tot de
groep mensen met een generieke monitor.

### Een Stukje Praktijk

Cloudflare zal je grote vijand worden als je clearnet sites bezoekt via de Tor
browser. Je zult regelmatig captchas van hun tegenkomen. Er is de optie om een
Cloudflare browser addon te installeren. **Doe dit _absoluut_ niet!** Cloudflare
is hiermee instaat te volgen op welke websites die je bezoekt.

Als je dan uiteindelijk dat 'enge' darknet fatsoenlijk wil gebruiken, zijn er
lijsten van links en search engines te vinden. Echter zit hier wel de
waarschuwing bij dat er op het Tor netwerk ook zeker afschuwelijke dingen te
vinden zijn. Maar dit is te vermijden als je je kennis van het gebruik van het
clearweb niet straal negeert. Op het Tor netwerk geldt nog steeds dezelfde regel
van "ga niet naar overduidelijk sketchy websites". Het voornaamste wat darknets
zijn is een ongefilterde blik op het internet.

De voornaamste link lijst die ik kan aanraden is de Tor lijst op
[LetsDecentralize.org](https://letsdecentralize.org/rollcall/tor.html)[(onion)](http://xanthexikes7btjqlkakrxjf546rze2n4ftnqzth6qk52jdgrf6jwpqd.onion/rollcall/tor.html).
Echter is ook _deze_ lijst niet alles; het script dat gebruikt wordt om te zien
of de desbetreffende onions nog in de lucht zijn, is niet 100% betrouwbaar. Dus
als je een site ziet die je wilt benaderen, check altijd zelf of deze in de
lucht is. Hierbij is het mogelijk dat je enkele minuten moet wachten tot je een
response krijgt van de site in kwestie.

### Tails

Een andere, veiligere manier om Tor te gebruiken is via Tails -- wat staat voor
The Amnesiac Incognito Live System.

Tails is een besturingssysteem wat al het netwerk verkeer over het Tor netwerk
stuurt. Dit is gecombineerd met de eigenschap die het heeft dat het niet
mogelijk is permanente veranderingen toe te passen aan het besturingssysteem.
Hiermee heb je een soort van incognito mode voor je hele computer; zodra je het
herstart krijg je weer een frisse omgeving.

Tails is bedoelt om op een USB stick of soortgelijk klein opslagmedium te
plaatsten. Na dit gedaan te hebben start je van deze USB stick -- en daarmee
Tails op. Via deze manier van je computer opstarten wordt alles wat je in deze
sessie doet volledig in RAM -- werkgeheugen geladen. De reden hiervoor is dat de
staat van RAM alleen wordt bijgehouden zolang als er stroom naar het
desbetreffende deel van de computer gaat. Zodra je de computer uitzet, wordt dus
alles wat er binnen de Tails sessie gebeurt is weer verwijderd.

Om Tor zo veilig mogelijk te gebruiken is het verstandig om _alles_ via Tails te
doen.

Tails is te downloaden [op hun eigen website](https://tails.net/). Instructies
zijn op hun website te vinden.

## i2p

Het Invisible Internet Protocol -- i2p netwerk lijkt veel op het Tor netwerk.
Cruciale verschillen zijn dat i2p helemaal geen centrale nodes heeft (Tor heeft
drie centrale nodes), dat er voor het beantwoorden van je request een andere set
van nodes gebruikt wordt. Hierdoor is het i2p netwerk een stuk trager; er worden
minimaal zes nodes gebruikt per interactie.

Als je een tijdje hebt rondgeneusd op Tor en meer blogs zou willen zien in
plaats van drugsmarkten en sites die beweren dat je een huurmoordenaar kunt
huren[^2], is i2p mogelijk interessant.

Echter is het inrichten van een i2p configuratie een stuk lastiger en vergt het
meer vrije tijd om ook maar een beetje rond te kunnen neuzen.

Wegens eerder genoemde redenen is het i2p netwerk iets wat ik vooral aanraad als
je al weet waar je mee bezig bent. Instructies zijn te vinden op [hun
webstie](https://geti2p.net).

[^1]: Een voorbeeld hiervan is hoe op mijn opleiding de nodige wiki's waar ik
    regelmatig gebruik van maak geblokkeerd werden.
[^2]: Een artikel over veel voorkomende browser/technologie misstanden en dergelijke is
    onderweg.
