---
title: "Adblockers"
date: 2023-07-26T18:45:28+02:00
author: Marty Sluijtman
documentSource: "https://gitlab.com"
tags: [ "convenience", "privacy", "internet"]
description: ""
toc: true
---

# Advertenties en het Tegenhouden Ervan

Ik snap niet hoe mensen instaat zijn om advertenties uit te staan. Wat ik nog
minder snap is dat mensen ook na het weten van bestaan van adblockers nog steeds
te lui zijn om een adblocker te installeren. Vandaar dit artikel. Het is
namelijk niet zo complex. Voor desktop browsers is het volgende lijstje wat ik
zelf ongeveer aanhoud qua adblockers.

```goat
                            +-- Firefox ----+
                            |               |
                            +-- LibreWolf --+
                            |               |
                            +-- Chromium ---+
                            |               |
                            +-- Vivaldi ----+-----> uBlock Origin
                            |               |
                            +-- Opera ------+
                            |               |
                            +-- Brave ------+----+
                            |                 +--+-> Ingebouwde adblock
Welke browser gebruik je? --+-- Qutebrowser --+
                            |                 +--> Vraag developer om ublock implementatie
                            +-- Google Chrome --+
                            |                   +--> Gebruik Brave of Firefox
                            +-- M$ Edge --------+
                            |
                            +-- Safari --> Goeie vraag... Ads van AdBlock Pro schijnt goed te zijn.
                            |
                            +-- Lynx --+
                            |          +--> Hoe krijg je hiermee advertenties te zien?
                            +-- W3m ---+
```

# uBlock Origin

uBlock Origin is een geweldig project wat op heel veel manieren te gebruiken is.
Zonder enige configuratie is het de beste ad- en trackerblocker die ik zo ken.
Dit komt omdat er constant gewerkt wordt aan de blokkeer lijsten die uBlock
heeft. Als je op hun subreddit --
[r/uBlockOrigin](https://old.reddit.com/r/uBlockOrigin/) gaat kijken zul je enorm
veel vragen tegenkomen om bepaalde elementen te blokkeren die vrij snel
afgehandeld worden. Sterker nog, bij het eens in de zoveel tijd doorscrollen van
de pagina's, ben ik niet één blokkeer vraag tegengekomen die nog niet was
afgehandeld. 

# Elementen Blokkeren

Je kunt het verder ook gebruiken om arbitraire elementen in websites te
blokkeren. Bijvoorbeeld als je favoriete nieuws site standaard een filmpje laat
zien ergens in een hoek wat je vrijwel nooit wilt zien, kun je het blokkeren.

Dit doe je door:
1. Er met de rechter muisknop op te klikken
2. Daar "Block element" te kiezen
3. Vervolgens krijg je een venstertje met wat meer informatie over het element
   wat je op het punt staat te blokkeren, "preview", "create", "pick" en "quit"
   knoppen. Persoonlijk vind ik de "pick" knop wat onduidelijk. Je kunt hiermee
   je selectie van element nog veranderen.
4. Kijk eerst even of je het gewenste element te pakken hebt met "preview".
5. Zo ja, klik op "create".

En viola, dat irritante filmpje is geblokkeerd.
