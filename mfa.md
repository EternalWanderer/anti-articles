---
title: "Multi Factor Authentication"
date: 2022-09-06T19:27:09+02:00
author: Marty Sluijtman
toc: true
tags: [ "security" ]
documentSource: "https://gitlab.com/EternalWanderer/anti-articles/-/blob/main/mfa.md"
---
# Dat extra stapje voor je account

Je bent het vast al wel een keer tegengekomen. Een app of service eist dat je
een via SMS toegestuurde code invult voordat je kunt inloggen om te verifiëren
dat jij het daadwerkelijk bent. Ik geef het een redelijke kans dat je er niet
echt veel over hebt nagedacht. In dagelijks gebruik is het tenslotte vooral
een extra hoepeltje om doorheen te springen voor dat je bij je account kunt. Van
een security perspectief daarentegen, is het een gigantische stap de juiste kant
op. Het houdt namelijk in dat het bemachtigen van een emailadres en wachtwoord
niet voldoende is om in je account in te breken.

De theorie hierachter is dat je iets wat je weet combineert met iets wat je hebt
om aan te tonen dat jij _jij_ bent. Het ding dat je weet is in dit geval je
wachtwoord en het ding dat je heb is je telefoon met de capaciteit om SMS
berichten te ontvangen.

Het probleem met deze vorm van MFA (Multi Factor Authentication) is echter dat
SMS geen veilig protocol is om cruciale informatie over te brengen. Het is om
te beginnen niet encrypted, het is in de Verenigde Staten voorgekomen dat
bedrijven advertenties injecteren in SMS berichten. Een ander probleem met SMS
is dat er het presedent is gezet van de zogenaamde SIM swap attack. Wat dit
inhoud is dat de offensieve partij jou SIM provider zo ver krijgt om hun een
SIMkaart te geven die hetzelfde nummer heeft als je huidige SIM kaart. Hiermee
krijgt de offensieve partij het antwoord van jou MFA query.

# TOTP

Een betere oplossing voor MFA is TOTP (Time based One Time Password). TOTP is
een open standaard die door de meeste bedrijven die MFA ondersteunen gebruikt
wordt. Het enige wat je ervoor nodig hebt is een applicatie die TOTP informatie
kan uitlezen en bewijs kan leveren dat je toegang hebt tot het eerder verkregen
shared secret. In het geval van TOTP is dat een seed die gebruikt wordt om een
serie van zes cijfers te genereren. Meestal veranderen die iedere 30 seconden,
vandaar "time based". Zolang je deze seed en een TOTP applicatie hebt kun je
altijd aan die cijfers komen. Met deze zes cijfers kun jij je authenticeren omdat
de server gebaseerd op dezelfde seed tot dezelfde cijfers zal komen.

> "Dit is allemaal leuk en aardig maar hoe moet ik beginnen met het gebruik van
TOTP?"

Er zijn theoretisch honderden TOTP applicaties, maar die zullen lang niet
allemaal voldoen aan iedereens eisen. Maar over het algemeen kun je er wel
vanuit gaan dat er _iets_ zal zijn wat er voor je werkt. Dit is ook een van de
weinige keren dat ik niet echt zal hameren op een specifieke
applicatie/implementatie. De eerste applicatie die ik aanraad is [Ente
Auth](https://ente.io/auth/). Als deze niet werkt voor jou usecase is er in
Android land nog [Aegis](https://getaegis.app/) te vinden. In tegenstelling tot
Ente Auth heeft Aegis geen account heeft nodig. Als dit _ook_ geen optie voor je
is, kun je nog altijd de authenticator van Google gebruiken. Mocht je jezelf
_echt_ haten, kun je ook nog altijd die van Microsoft gebruiken.

* Als je eenmaal een app geïnstalleerd hebt is het verder een kwestie van bij
  een account TOTP aan te zetten. Dit is meestal ergens te vinden onder de
  security settings en daaronder 2FA of MFA.
* Hier zul je een QR code te zien krijgen die je met je app moet scannen.
* Daarna vraag je website om de code die je terug krijgt in je app in te vullen
  op de website.
* Vervolgens krijg je een aantal backup codes voor het geval dat je toegang tot
  je TOTP applicatie kwijtraakt. Sla deze goed op. Print ze bijvoorbeeld uit en
  leg ze op een plek waarvan je weet dat ze niet makkelijk ergens anders zullen
  belanden. Als je ze niet wilt uitprinten raad ik aan om ze ergens encrypted op
  te slaan [^1].

Er zijn hier nu een paar dingen gebeurt die cruciaal zijn bij het begrijpen
waarom TOTP een fantastische standaard is.

Bij het scannen van de QR code, heb je de informatie die die QR code bevat, in
je TOTP database gegooid. De informatie van die QR code is de seed die gebruikt
wordt om vervolgens je zes cijfers te generen. Die seed heeft ongeveer de
volgende structuur:
```
otpauth://totp/<account-informatie><website.com>&secret=AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA&issuer=<website>
```
Het kan zijn dat er nog meer delen bijzitten als `algorithm=SHA1` of `digits=6`
maar de kern is de account informatie en het secret.

Sommige websites laten je ook de secret direct buiten de QR code zien zodat je
het over kunt typen.

Dit brengt mij terug bij een punt waar TOTP applicaties op kunnen falen. Dat is
de mogelijkheid om je seed weer uit de applicatie te kunnen halen. Als dat niet
mogelijk is, zit je vervolgens vast aan die applicatie en zul je of die
applicatie moeten blijven gebruiken of een nieuwe seed aanvragen. Een manier om
hier omheen te werken is overigens om altijd zelf een kopie te hebben van je
seed als bv een notitie in je [passwordmanager](/passwordmanagers). Je kunt een
kopie maken door de QR code eerste met een andere app te scannen, [Binary
Eye](https://play.google.com/store/apps/details?id=de.markusfisch.android.binaryeye&gl=US)
is wat ik hiervoor aanraad.

[^1]: TODO: artikel over file encryptie.
