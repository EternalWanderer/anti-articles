---
date: 2022-09-11T13:37:36+02:00
author: Marty Sluijtman
description: "Wiki met basis informatie voor de leek op gebied van moderne, digitale technologie"
title: Documents from the Antiverse
---
Er zijn regelmatig situaties waar er een complex concept naar boven komt wat
behoorlijk wat tijd vergt om fatsoenlijk uit te leggen. Ik heb over het algemeen
het geduld om de concepten uit te leggen. Het probleem daarentegen is dat het op
een gegeven moment begint te lijken op een lezing. De meeste mensen hebben geen
behoefte aan een lezing als het gaat om voor het eerst kennis maken met een
concept.

Daarnaast wil ik graag de nadruk leggen op "..._algemeen_ het geduld..." Ik wil
ook de mogelijkheid hebben mensen te helpen met iets als ik zelf geen tijd en/of
zin heb.

### Ik wil graag contribueren aan deze site.

Stuur een mailtje [mijn richting op](mailto:marty.wanderer@disroot.org) met
een samenvatting van waarover je het wilt hebben en/of maak een pull request aan
met je artikel in de [GitLab
repository](https://gitlab.com/EternalWanderer/anti-articles).
