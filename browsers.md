---
title: "Browsers"
date: 2022-09-07T22:43:54+02:00
author: Marty Sluijtman
documentSource: "https://gitlab.com/EternalWanderer/anti-articles/-/blob/main/browsers.md"
tags: [ "internet", "privacy", "security" ]
description: "Haken en ogen aan browsers in relatie tot moderne internet"
toc: true
---

# Introductie

[Statistisch](https://www.w3schools.com/browsers/default.asp) gezien gebruik je
Google Chrome om dit artikel te lezen. Van een puur technisch perspectief
gezien is er niks mis het deze browser. Google Chrome is zeer goed instaat om
pagina's best netjes en snel te renderen. Er zijn daarentegen een aantal
redenen dat Google Chrome niet bepaald de beste keuze is die je kunt maken.
Één van die redenen is Googles implementatie van een post-cookie tracking
protocol en het feit dat alleen Google het een goed idee vind.

> "Een post-wattes wat protocol?"

Cookies worden gebruikt om dingen bij te houden die gerelateerd zijn aan waar je
in je browser mee bezig bent. Dit zijn onder andere dingen als de sites waar je
ingelogd bent zodat je ook daadwerkelijk ingelogd blijf nadat je naar een andere
pagina gaat of een keer op F5 drukt om je huidige pagina te herladen. Dit
mechanisme werkt door uit te lezen wat voor cookies je browser heeft en
vervolgens te kijken of je een cookie hebt wat aantoont dat je je username en
password [^1] hebt ingevuld en daarmee bewijst dat je een valide inlog sessie
hebt. Het probleem hiermee is dat websites hun desbetreffende cookies die je
browser heeft _altijd_ kunnen uitlezen. Als je ingelogd bent op site X, Y en Z;
en er word gerefereerd naar site Y en Z in de code van X, is het mogelijk voor Y
en Z om bij te houden dat je op X geweest bent. Dit gebeurt omdat de eigenaar
van X, code van site Y en Z de privilege geeft om te draaien op zijn site. [^2]

In deze context zijn de cookies van site Y en Z third-party cookies. In andere
woorden, cookies die niet direct gerelateerd zijn aan de website waar je
momenteel bent. Het is inmiddels algemeen bekend dat sites als Facebook
bijhouden waar je bent geweest door middel van een stukje code wat in die "Like
us on Facebook" knoppen zit wat je `facebook.com` cookies uitleest. Hierdoor
hebben de meeste browsers tegenwoordig standaard de optie aanstaan om het
uitlezen third-party cookies niet toe te staan. Dit wordt gedaan door de cookies
van iedere site waar je heengaat geïsoleerd op te slaan. Hiermee heb je nog
steeds de nuttige functionaliteit van cookies maar zonder het bijverschijnsel
dat je een spoor van je activiteit achterlaat.

> "Wat heeft dit hele verhaal met Google (Chrome) te maken?"

Google Chrome is zo goed als de enige browser op de markt die standaard absoluut
niet aan third-party cookie isolatie doet. Dit komt omdat ge-/misbruik van
third-party cookies een gigantisch deel is van Googles verdienmodel. In andere
woorden, omdat Google jou graag advertenties voor wilt houden, staan ze het ook
toe voor andere bedrijven om je internet spoor te bij houden.

## FLoC-ing hell

**2023-07-26 Update**: Wegens de algemene middelvingers van de grootste spelers
van het tech sector, is het FLoC project onder deze naam niet echt relevant
meer. De principes daarentegen zijn vrijwel niet veranderd.

Omdat de meeste browsers tegenwoordig third-party cookies zo goed als nutteloos
hebben gemaakt en bedrijven als Google en Facebook nog steeds graag aan geld via
advertenties willen komen, zijn er een aantal alternatieve ideeën naar voren
gebracht. Googles implementatie genaamt Federated Learning of Cohorts, afgekort
naar FLoC (wat tot "hilarische" headlines leidde als "FLoC off, Google") kreeg
als eerste enorm veel aandacht omdat het Google in een centrale positie plaatst
ten aanzien van gebruikersinformatie.

Het protocol werkt als volgt; in je locale browser configuratie wordt er
bijgehouden waneer en waar je welke sites je bezoekt. Deze websites zitten in
verschillende categorieën van content, in andere woorden, cohorts. Vervolgens
wordt enige persoonlijke informatie eruit gefilterd en word het resultaat naar
Google opgestuurd.

Wat onder persoonlijke informatie valt lijkt overigens op het niveau van
voornaam, achternaam en geboortedatum te zitten. Geen van deze informatiepunten
is nodig om een duidelijk beeld te krijgen van wie iemand is. De sites die
iemand bezoek creëren veel makkelijker een unieke schets van een persoon.

Het resultaat van FLoC is nog indringender dan het huidige systeem van
third-party cookies. De reden dat andere grote bedrijven al openlijk hebben
gezegd dat ze FloC niet zullen ondersteunen is omdat ze Google effectief
monopolie zouden geven over het verzamelen van informatie voor advertenties.

Om het nog mooier te maken is Google bezig om FLoC te testen op een bepaald
deel van Chrome gebruikers zonder de gebruikers in te lichten.

Voor een concretere uitleg over FLoC kan ik <https://amifloced.org/> van de EFF
van harte aanraden. Het raakt want andere punten aan dan wat ik hier beschreven
heb en geeft een algeheel betere uitleg.

# Andere Browsers dan Google Chrome

> "Wat zijn dan betere browsers om te gebruiken dan Google Chrome"

Eigenlijk alles wat niet Google Chrome is, is al beter. Zelfs Chromium, de Open
Source basis van Google Chrome, is al beter qua privacy en integriteit naar
gebruikers. Sterker nog, Microsoft Edge is al beter dan Google Chrome aangezien
het geen FLoC ondersteund en in ieder geval standaard de _optie_ heeft om
third-party cookies te blokkeren.

## Brave

Als je niet echt bereid bent om tijd te stoppen in het configureren van je
browser is de eerste die ik aanraad [Brave](https://brave.com). Brave heeft
standaard zichzelf een ingebouwde ad- en trackerblocker die het _redelijk_ doet
over het algemeen. Het is een implementatie van [uBlock Origin](#ublock-origin)
met een Brave-specifieke lijst en een simpelere interface.

Ik voel me daarentegen verplicht te zeggen dat Brave een enorm duidelijk
pro-crypto currency[^3] browser is. Standaard krijg je soms wat advertenties te
zien waarvoor je dan betaald wordt in Brave's eigen crypto currency: BAT. Het
probleem hiermee is dat je 1. Standaard advertenties te zien krijgt in een
browser die pretendeerd advertenties te blokkeren en 2. Het enorm moeilijk is om
BAT naar enige fiat currency[^4] te converteren. Daarnaast is het enige platform
waarmee je BAT uit je Brave wallet kunt krijgen Coinbase, wat per se een portret
van je wilt hebben bij het aanmaken van je account. Gelukkig kun je alle crypto
gerelateerde menuutjes wegstoppen en houd je een prima browser over.

## Firefox

[Firefox](https://www.mozilla.org/en-US/firefox/new/) is een browser waar enorm
veel aan veranderd kan worden. Standaard is het geen geweldige browser, maar dit
is makkelijk te veranderen door een paar settings anders in te stellen. Als
eerste raad ik aan om het in het settings menu naar "Privacy & Security"  te
gaan, daar naar beneden te scrollen en onder de kop "Firefox Data Collection and
Use" alles uit te zetten. Vervolgens raad ik aan om "HTTPS-Only Mode" op alle
vensters aan te zetten. Tot slot is er "Enhanced Tracking Protection" boven aan
de "Privacy & Security" pagina. Ik raad aan deze op "Strict" te zetten.

Dit menu is overigens sneller te bereiken door `about:config#privacy` in de URL
balk te typen/kopiëren.

Verder is er één plugin die is als essentieel beschouw voor een basis Firefox
configuratie en dat is [uBlock Origin](https://ublockorigin.com/). Voor een
basis configuratie hoef je er verder niks aan te doen.

### Geavanceerde configuratie

Als je een stap verder wilt nemen en geen gebruik wilt maken van andere
browsers -- zij het een Firefox fork of een Chromium browser -- kan ik [Project
Arkenfox](https://github.com/arkenfox/user.js/wiki) van harte aanraden.

## LibreWolf

[LibreWolf](https://librewolf.net/) is gebaseerd op Firefox met behoorlijk
strikte security en privacy policies. Is mogelijk wat aan de heftige kant als je
niet weet waar je mee bezig bent. Het houd bijvoorbeeld geen browser
geschiedenis bij. Daarnaast geldt overigens nog steeds dezelfde regel van [uBlock
Origin](https://ublockorigin.com/) installeren.

## Mullvad Browser

[Mullvad](https://mullvad.net) is een VPN bedrijf wat besloten heeft om met het
Tor project samen te werken om een nieuwe Firefox fork te maken. Het resultaat
hiervan is een browser die voor een groot deel dezelfde hardening features heeft
als de Tor browser. Let als LibreWolf en de Tor browser houd de [Mullvad
browser](https://mullvad.net/en/download/browser/windows) geen history of
cookies of wat dan ook bij.

In tegenstelling tot de Tor browser maakt het geen gebruik van enige vorm van
een [darkweb](/darkweb) of iets in die geest. In plaats daarvan zit er een
plugin in on je Mullvad VPN connectie makkelijk vanuit de browser bij te kunnen
houden. Er vanuitgaand dat je ook Mullvad VPN gebruikt. Echter is dit niet nodig
en is deze extentie makkelijk te verwijderen.

[^1]: Hier een artikel over het bijhouden van passwords [Passwordmanagers](/passwordmanagers)
[^2]: Als je _echt_ paranoïde wilt worden moet je eens kijken hoe vaak dat je
  referenties naar bv `googletagmanager.com` te zien krijgt met
  [NoScript](https://noscript.net/) of [uBlock Origin](https://ublockorigin.com/).
[^3]: Artikel over crypto currencies komt nog ergens.
[^4]: Fiat is de benaming voor "echte" vormen van geld, denk aan de Euro,
  Dollar en Pond.
