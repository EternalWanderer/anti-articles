---
title: "Passwordmanagers"
date: 2022-09-06T10:28:11+02:00
author: Marty Sluijtman
documentSource: "https://gitlab.com/EternalWanderer/anti-articles/-/blob/main/passwordmanagers.md"
tags: [ "security", "convenience" ]
description: "De oplossing voor wachtwoorden vandaag de dag"
toc: true
---

# Wachtwoord irritaties

Je kent het wel, je maakt een nieuw account aan en krijgt een wachtwoord veldje
te zien als deel van de procedure. Misschien heb je wel van je locale IT nerd
gehoord dat je voor ieder account een lang en uniek wachtwoord moet gebruiken.
Het probleem daarmee is dat het al moeilijk genoeg is om drie wachtwoorden te
onthouden, laat staan vijftig of zelfs honderd. Uiteindelijk kom je dus weer
terug op dat wachtwoord wat je tien jaar geleden voor het eerst gebruikte bij
het aanmaken van je huidige emailaccount. Met een beetje geluk heb je toen al
de helderheid van geest gehad om niet "password" of "password123" te gebruiken.
Er is ook een redelijke kans dat je de naam van een naaste hebt gebruikt,
misschien zelfs met een oplopend nummer aan het eind met ieder nieuw account
wat je aanmaakt. Dit gebeurt regelmatig met het doel een wachtwoord te hebben
wat je makkelijk kunt onthouden.

Het probleem met "password" en "password123" is dat ze tot de meest gebruikte
wachtwoorden ter wereld horen. Hetzelfde geldt voor makkelijk te raden patronen
als de namen van naasten of je geboortedatum.

Misschien ben je zo iemand die doorheeft dat hetzelfde password recyclen een
slecht idee is en dus een boekje gebruikt om al je wachtwoorden bij te houden.
Maar dit is niet heel erg gemakkelijk om te doen. Ja, je hoeft inderdaad niet
meer tientallen wachtwoorden in je hoofd te houden, maar een boekje
doorbladeren is niet bepaald iets wat je effe gauw snel doet. De meeste mensen
doen het dan ook niet. De rest van dit document gaat erover hoe je wachtwoorden
op een makkelijke én veilige manier kunt bijhouden.

# Passwordmanagers

> "Wat is deze magische oplossing voor dit afschuwelijke probleem?" vraag je je
nu af.

Één woord: passwordmanagers.

Het enige wat je voortaan hoeft te onthouden is je zogeheten master password.
Dit is de sleutel voor de digitale kluis waar al je wachtwoorden inzitten.
Vanuit deze kluis kan je passwordmanager zelfs wachtwoorden automatisch
invullen, mocht je dat fijn vinden. Dit maakt het mogelijk om gemakkelijk
gigantische unieke hoeveelheden wachtwoorden bij te houden. De meeste
passwordmanagers hebben zelfs een genereer knop. Hierdoor hoef je niet eens een
wachtwoord te bedenken.

> "Hoe kom ik aan zo'n digitaal Zwitsers zakmes voor wachtwoorden?" gaat er nu
door je hoofd.

## Bitwarden

Voor de meeste mensen raad ik [Bitwarden](https://bitwarden.com) aan. Het heeft
een duidelijke interface en kost niks voor alles wat de meeste mensen nodig
zullen hebben. De andere eigenschap waardoor ik Bitwarden aanraad is de sync
functionaliteit tussen alle apparaten waar je het op installeert. Als je een
wachtwoord aanmaakt via de telefoon app, is het ook te gebruiken via je
computer.

> "Waarom zou ik Bitwarden vertrouwen met mijn wachtwoorden? Zijn zij dan niet
instaat mijn wachtwoorden te lezen?"

Nee. Je wachtwoord kluis is beveiligd met zogeheten asymmetrische encryptie.
Wat dit inhoud is dat er voor het openen van je kluis per se een sleutel nodig
is die alleen te gebruiken is na het meegeven van je master password.

> "Hoe begin ik met het gebruiken van Bitwarden?"

1.  Ga naar <https://bitwarden.com> en klik op "Get started today".
2.  Vul informatie in naar wens tot je bij het "Master Password" veld komt.
3.  Voor de inhoud van een master password raad ik aan om een zin te gebruiken.
    Zorg ervoor dat je master password op z'n minst zestien karakters is. Een
    veel voorkomend verhaal wat je zult horen is dat het ook complex moet zijn.
    Dit is minder belangrijk, zolang het maar lang en niet voorspelbaar is.
    Naast het gebruik van een zin is het ook mogelijk om een stuk of vijf
    behoorlijk lange woorden aan elkaar te hangen die niks met elkaar te maken
    hebben. Het klassieke voorbeeld is "[correct horse battery
    staple](https://xkcd.com/936)". Gebruik dat specifiek daarentegen niet,
    aangezien het regelmatig als voorbeeld wordt gebuikt.
4.  Vul bij password hint iets in wat je doet denken aan wat je wachtwoord is,
    zolang het maar niet hetzelfde is als het wachtwoord. Als je bijvoorbeeld
    een Starwars gerelateerd password hebt en je vind de laatste drie films
    beste, in tegenstelling tot de populaire interpretatie, zou je "drie
    laatste sterren ketters" kunnen gebruiken.
5.  Verifieer je emailadres.

Gefeliciteerd, je hebt nu een werkend Bitwarden account!

> "Hoe maak ik een wachtwoord aan?"

1.  Klik op "Add Item"
2.  Verder zijn hier de stappen redelijk vanzelfsprekend.
3.  Het wordt interessant bij "URI 1" en "Match Detection".\
    Deze velden zijn voor het automatisch invullen van wachtwoorden.\
    Bij "URI 1" kun je de URL van de desbetreffende website invullen.\
    Bij "Match Detection" kun je, als je weet wat je doet, kiezen om Bitwarden
    op een andere manier de URL in je browser af te zoeken.

Verder is het mogelijk om wachtwoorden te genereren door op de pijltjes in een
cirkeltje bij het "Password" veldje te klikken.

Vanuit de app en browser plugin is het mogelijk om ieder keer dat je inlogt,
het wachtwoord automatisch op te slaan in je kluis. Dit is de makkelijkste
manier naar mijn ervaring om correcte informatie te krijgen bij in de URI
veldjes.

> "Je heb het nu al een paar keer over apps en browser plugins gehad, waar kan
ik deze vinden?"

Er is [een lijstje](https://bitwarden.com/download) te vinden voor browser
plugins en desktop-en-telefoon applicaties.

> "Ik wil **toch** niet dat mijn wachtwoorden op een publieke server
terechtkomen"

Dan heb je een paar opties. De eerste is het makkelijkst als je lokale nerd
bereid is om zijn nerd kennis voor jou toe te passen. Dat is namelijk om een
[Vaultwarden](https://github.com/dani-garcia/vaultwarden) server te draaien
ergens. Vaultwarden is een lichtere implementatie van de officiële Bitwarden
server die volledig compatibel is met de officiële applicaties. Voordeel aan
Vaultwarden is dat je gratis de premium features krijgt. Het handigst van deze
is voor mij tot nu toe de OTP client implementatie geweest.

Bij het inloggen bij de applicaties, staat er linksboven aan een settings
knopje in de vorm van een tandwieltje. Als je daar op klikt krijg je een hoop
veldjes te zien. Hier moet je de URL van je privé server in het "Server URL"
veldje gooien. Vervolgens kun je dan inloggen met het account wat je heb
aangemaakt op de privé server. Als je nog geen account hebt hier, kun je ook
vanuit de app een account aanmaken.

## KeePassXC

De andere optie is [KeePassXC](https://keepassxc.org).

KeePassXC maakt gebruik van een lokale wachtwoord database die je zelf bij moet
houden als je het op meerdere devices wilt hebben. Daarnaast heeft het geen
premium versie van en kun je dus sowieso alle features gebruiken.

Aan de andere kant heeft KeePassXC heel wat meer features, waaronder een heel
veel complexere password generator en een ssh agent (als je niet weet wat dit
is, kun je het vrolijk negeren). De password generator van KeePassXC is ook
instaat om passphrases te genereren. Passphrases, in tegenstelling tot
traditionele passowrds hebben het voordeel dat ze bestaan uit woorden, vandaar
pass "phrase". Het voordeel hieraan is dat ze makkelijker te onthouden zijn dan
gegenereerde passwords van vergelijkbare veiligheid.

KeePassXC geeft je ook de optie om een keuze te maken over wat voor encryptie
algorithme te gebruiken. Op het gebied van security heeft het ook de optie om
een Yubikey en/of keyfile te gebruiken als multi factor authentication [^1]
methodes.

Verder heeft KeePassXC ook browser intigratie die je in de desktop client aan
en uit kunt zetten per browser.

Lijst aan ondersteunde browsers in de desktop client is als volgt:

- Google Chrome
- Chromium
- Firefox
- Vivaldi
- Tor Browser
- Brave
- M$ Edge

Naar mijn ervaring werkt het ook prima met Firefox afsplitsing LibreWolf en
Chromium afsplitsing UnGoogled Chromium. [^2]

Een ander noemenswaardig feature is de controle die je krijgt over je OTP [^1]
tokens. KeePassXC heeft als optie om voor Valve's Steam platform MFA bij te
houden.

Desondanks is het goed om te weten dat het opslaan van MFA informatie op de PC
kwa security wat zwakker is aangezien de meeste PC's niet zo goed beveiligd
zijn als telefoons. 

## Pass en GoPass
_**DISCLAIMER:**_ **het volgende deel is gericht op eeder genoemde IT nerds**

> "Deze projecten zijn allemaal leuk en aardig, maar ik heb liever iets
minimalistischer."

In dat geval heb je nog altijd
[Pass](https://www.passwordstore.org/)[\(git\)](https://git.zx2c4.com/password-store/)
en
[GoPass](https://www.gopass.pw/)[\(git\)](https://github.com/gopasspw/gopass/).
Als je bij let lezen van de introductie paragrafen hiervan op hun
respectievelijke sites niet gillend wegrent, ga ik van uit dat je redelijk wat
kennis hebt van Unix systemen, Git en GPG.

### Pass
Deze twee passwordmanagers zijn begonnen bij Pass. Pass is een bash script wat
een directory aan GPG encrypted files automatisch bijhoud via Git. Het mooie
hieraan is dat je je eigen Git server kunt gebruiken om je passwords bij te
houden.  Het mooie aan de simpliciteit van Pass is dat men er heel wat
implementaties en wrappers en extenties rondom heeft geschreven.

Het initialiseren van een vault is te doen met het volgende:
```sh
pass init <gpg-fingerprint>
```
Vervolgens voeg je een password toe met:
```sh
pass add <password-name>
```
Een lijst van passwords kun je opvragen met met:
```sh
pass
```
Een password opvragen doe je met:
```sh
pass <password-name>
```
Het is ook mogelijk om passwords te genereren met:
```sh
pass generate <password-name> [password-length]
```
Passwords aanpassen en rename-en gaat met:
```sh
pass edit <password-name>
pass mv <password-name> <new-password-name>
```

Pass houd je passwords bij in een git repository. Dit gaat grotendeels
automatisch, maar als je met de hand de repo wilt manipuleren doe je dat door
`pass` voor de standaard git commando's te gooien. Commits worden automatisch
bijgehouden.

```sh
pass git pull
pass git push
pass git remote add origin <git-bare-repo>
pass git config --edit
```

Pass is te vinden in de standaard repos van de volgende Linux distros:

- [Alpine](https://pkgs.alpinelinux.org/packages?name=pass&branch=edge)
- [Arch](https://archlinux.org/packages/community/any/pass)
- [Debian](https://packages.debian.org/sid/pass)
- [Fedora](https://packages.fedoraproject.org/pkgs/pass/pass)
- [Gentoo](https://packages.gentoo.org/packages/app-admin/pass)
- [Nix](https://search.nixos.org/packages?channel=unstable&show=pass&size=1&type=packages&query=pass)
- [OpenSUSE](https://software.opensuse.org/package/pass)

### GoPass
GoPass is een superset van Pass met de optie om meerdere vaults bij te houden,
de optie voor [Age](https://github.com/FiloSottile/age) in plaats van GPG, de
optie voor [Fossil](https://www2.fossil-scm.org/home/doc/trunk/www/index.wiki)
in plaats van git, automatische syncing iedere week, een REPL interface en
ingebouwde OTP support.

De syntax van GoPass is grotendeels hetzelfde als die van Pass en is backwards
compatible met Pass.

GoPass is te vinden in de standaard repos van de volgende Linux distros:

- [Alpine](https://pkgs.alpinelinux.org/packages?name=gopass&branch=edge)
- [Arch](https://archlinux.org/packages/community/x86_64/gopass)
- [Fedora](https://packages.fedoraproject.org/pkgs/gopass/gopass)
- [Gentoo](https://packages.gentoo.org/packages/app-admin/gopass)
- [Nix](https://search.nixos.org/packages?channel=unstable&show=gopass&size=1&type=packages&query=gopass)
- [RedHat](https://copr.fedorainfracloud.org/coprs/daftaupe/gopass/)

In het geval de [Debian repo zitten er wat haken en ogen aan](https://github.com/gopasspw/gopass/issues/1849#issuecomment-802789285).

[^1]: Hier [een artikel](/mfa) over Multi Factor Authentication.
[^2]: Hier [een artikel](/browsers) over browsers.
